<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
        'quantity',
        'shipping_method',
        'purchase_location',
        'delivery_location',
        'price',
        'user_id',
        'traveller_id',
    ];
//
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function traveller()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

}
