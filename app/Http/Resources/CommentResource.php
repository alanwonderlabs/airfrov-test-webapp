<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'request_id' => $this->request_id,
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'created_at' => $this->user->created_at,
                'updated_at' => $this->user->updated_at
            ],
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
