<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RequestResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'quantity' => $this->quantity,
            'shipping_method' => $this->shipping_method,
            'purchase_location' => $this->purchase_location,
            'delivery_location' => $this->delivery_location,
            'price' => $this->price,
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name
            ],
            'traveller' => [
                'id' => $this->traveller->id,
                'name' => $this->traveller->name
            ],
            'status' => [
                'name' => $this->status->name,
                'description' => $this->status->description
            ],
            'comments' => $this->comments,
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
