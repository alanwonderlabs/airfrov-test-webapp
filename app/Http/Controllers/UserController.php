<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Request;
use App\Models\Review;
use App\User;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function recentRequest(User $user)
    {
        return Request::where('user_id', $user->id)
            ->orderByRaw('updated_at - created_at DESC')
            ->get();
    }

    public function reviews(User $user)
    {
        return Review::where('user_id', $user->id)->get();
    }
}