<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
        $review = Review::create($request->all());
        return response()->json($review, 201);
    }

    public function update(Request $request, Review $review)
    {
        $review->update($request->all());
        return $review;
    }

    public function delete(Request $request, Review $review)
    {
        $review->delete();
        return response()->json(null, 204);
    }
}