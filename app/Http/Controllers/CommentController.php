<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    public function show(Comment $comment)
    {
        return $comment;
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->all());
        return $comment;
    }

    public function delete(Request $request, Comment $comment)
    {
        $comment->delete();
        return response()->json(null, 204);
    }
}