<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentResource;
use App\Http\Resources\RequestResource;
use App\Models\Comment;
use App\Models\Request;
use App\User;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request as RequestData;

class RequestController extends Controller
{
    public function show(Request $request)
    {
        return new RequestResource($request);
    }

    public function storeComment(RequestData $request)
    {
        $comment = Comment::create($request->all());
        return response()->json($comment, 201);
    }

    public function showComment(Request $request)
    {
        return CommentResource::collection(Comment::where('request_id', $request->id)->get());
    }

    public function store(RequestData $requestData)
    {
        $request = Request::create($requestData->all());
        return response()->json($request, 201);
    }

    public function update(RequestData $requestData, Request $request)
    {
        $request->update($requestData->all());
        return $request;
    }

    public function delete(Request $request)
    {
        $request->delete();
        return response()->json(null, 204);
    }
}