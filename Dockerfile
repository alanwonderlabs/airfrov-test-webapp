FROM php:7.0-apache

MAINTAINER Wonderlabs <alan@wonderlabs.io>

RUN apt-get update && apt-get install -y wget libxrender1 libfontconfig1 libmemcached-dev libz-dev libxml2-dev \
libmcrypt-dev mcrypt openssl zip unzip git libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng12-dev libssl1.0.0 net-tools

RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql mcrypt mysqli mbstring xml tokenizer \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN pecl install memcached

RUN docker-php-ext-enable memcached

RUN a2enmod rewrite

RUN a2enmod ssl

RUN rm -r /var/lib/apt/lists/*

COPY docker/php/php.ini /usr/local/etc/php/
COPY docker/apache/000-default.conf /etc/apache2/sites-available/

COPY . /var/www/html/

EXPOSE 9000