# AIRFROV TEST

# Tech
  - Laravel

# Requirements
  - PHP 
  - MySql
  
# Running
        
Install dependancies use composer :

        $ composer install
        
To run app :

        $ php artisan serve --port=9090
        
To run migration and seeder :

        $ php artisan migrate:refresh --seed

(rollback and re-run all of your migrations)

# API Resources

  - [Request](#request)

## Request

### Show Request By Id

    GET localhost:9090/api/requests/{id}

Using :

    curl --request GET localhost:9090/api/requests/1

Response :

    {
      "data": {
        "id": 1,
        "name": "Orihiro Night Diet Teh ut Jointripnuri",
        "description": "Night diet tea (1 pak isi 20 teh celup) Titip 14 orihro night diet tea Tolong cek expired terjauh ya. Kalau bisa traveler kembali jadetabek ya supaya bisa popbox. Prefer tiba seblm mid jan 2018 Utk mba Nuri",
        "quantity": 1,
        "shipping_method": "Kurir",
        "purchase_location": "Jepang",
        "delivery_location": "Jadetabek",
        "price": 100000,
        "user": {
          "id": 1,
          "name": "dessyxcamui"
        },
        "traveller": {
          "id": 2,
          "name": "jointripnuri"
        },
        "status": {
          "name": "Pending Purchase",
          "description": "Requester telah membayar deposit dan barang belum dibeli oleh traveller."
        },
        "comments": [
          {
            "id": 1,
            "description": "Masih available ?",
            "request_id": 1,
            "user_id": 2,
            "created_at": "2018-04-12 06:06:34",
            "updated_at": "2018-04-12 06:06:34"
          },
          {
            "id": 2,
            "description": "Masih bisa nitip? bisa pesan banyak nggak ya?",
            "request_id": 1,
            "user_id": 3,
            "created_at": "2018-04-12 06:06:34",
            "updated_at": "2018-04-12 06:06:34"
          }
        ],
        "created_at": "2018-04-12 06:06:34",
        "updated_at": "2018-04-12 06:06:34"
      }
    }
    
    
### Show Comments By Request Id

    GET localhost:9090/api/requests/{id}/comments

Using :

    curl --request GET localhost:9090/api/requests/1/comments

Response :

    {
      "data": [
        {
          "id": 1,
          "description": "Masih available ?",
          "request_id": 1,
          "user": {
            "id": 2,
            "name": "jointripnuri",
            "created_at": null,
            "updated_at": null
          },
          "created_at": "2018-04-12 06:06:34",
          "updated_at": "2018-04-12 06:06:34"
        },
        {
          "id": 2,
          "description": "Masih bisa nitip? bisa pesan banyak nggak ya?",
          "request_id": 1,
          "user": {
            "id": 3,
            "name": "alandwiprasetyo",
            "created_at": null,
            "updated_at": null
          },
          "created_at": "2018-04-12 06:06:34",
          "updated_at": "2018-04-12 06:06:34"
        }
      ]
    }
    
    
    
## Comments
### Create Comment By Request Id

    POST localhost:9090/api/requests/{id}/comments

Using :

    curl --request POST localhost:9090/api/requests/1/comments -H "Content-type: application/json" --data "{ \"description\": \"this is comment\", \"request_id\": 1, \"user_id\" : 2 }"


Request Example :

    {
        "request_id": "1",
        "user_id": "2",
        "description": "this is comment description",
        "id": 4
    }

### Edit comment

    PUT localhost:9000/comments/{id}

Using :

    curl --request PUT localhost:9090/api/comments/1 -H "Content-type: application/json" --data  "{ \"description\": \"this is comment\" }"


Request example:

    {
        "description": "this is comment description",
    }

Response :

    {
      "id": 1,
      "description": "this is comment",
      "request_id": 1,
      "user_id": 2,
      "created_at": "2018-04-12 06:06:34",
      "updated_at": "2018-04-12 06:28:47"
    }

### Delete Comment

    DELETE localhost:9090/api/comments/{id}

Using :

    curl --request DELETE localhost:9090/api/comments/1


## For more detail routes :

![alt text](routes.png)


