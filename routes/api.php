<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('comments')->group(function () {
    Route::get('/', 'CommentController@index');
    Route::get('/{comment}', 'CommentController@show');
    Route::put('/{comment}', 'CommentController@update');
    Route::delete('/{comment}', 'CommentController@delete');
});


Route::prefix('requests')->group(function () {
    Route::get('/{request}', 'RequestController@show');
    Route::get('/{request}/comments', 'RequestController@showComment');
    Route::post('/{request}/comments', 'RequestController@storeComment');
    Route::post('/', 'RequestController@store');
    Route::put('/{request}', 'RequestController@update');
    Route::delete('/{request}', 'RequestController@delete');

});

Route::prefix('reviews')->group(function () {
    Route::post('/', 'RequestController@store');
    Route::put('/{review}', 'RequestController@update');
    Route::delete('/{review}', 'RequestController@delete');
});

Route::prefix('users')->group(function () {
    Route::get('/{user}', 'UserController@show');
    Route::get('/{user}/requests', 'UserController@recentRequest');
    Route::get('/{user}/reviews', 'UserController@reviews');
});