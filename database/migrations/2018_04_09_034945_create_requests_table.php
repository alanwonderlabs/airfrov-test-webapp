<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description');
            $table->integer('quantity');
            $table->string('shipping_method');
            $table->string('purchase_location');
            $table->string('delivery_location');
            $table->integer('price');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('traveller_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('traveller_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropForeign(['traveller_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('requests');
    }
}
