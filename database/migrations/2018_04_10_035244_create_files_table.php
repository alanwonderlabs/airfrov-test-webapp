<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('size');
            $table->string('name');
            $table->string('original_name');
            $table->string('description');
            $table->string('type');
            $table->string('mime_type');
            $table->string('source');
            $table->string('source_type');
            $table->integer('owner_id')->nullable()->unsigned();
            $table->foreign('owner_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            //
            $table->dropForeign(['owner_id']);
        });
        Schema::dropIfExists('files');
    }
}
