<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'dessyxcamui',
                'email' => 'dessyxcamui@gmail.com',
                'password' => bcrypt('password'),
                'about' => 'This is about dessyxcamui',
                'verification' => true
            ],
            [
                'name' => 'jointripnuri',
                'email' => 'jointripnuri@gmail.com',
                'password' => bcrypt('password'),
                'about' => 'This is about me',
                'verification' => true
            ],
            [
                'name' => 'alandwiprasetyo',
                'email' => 'alandwi@test.com',
                'password' => bcrypt('password'),
                'about' => 'This is about alan',
                'verification' => true
            ]
        ];

        DB::table('users')->insert($users);
    }
}