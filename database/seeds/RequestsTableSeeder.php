<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reviews = [
            [
                'name' => 'Orihiro Night Diet Teh ut Jointripnuri',
                'description' => 'Night diet tea (1 pak isi 20 teh celup) Titip 14 orihro night diet tea Tolong cek expired terjauh ya. Kalau bisa traveler kembali jadetabek ya supaya bisa popbox. Prefer tiba seblm mid jan 2018 Utk mba Nuri',
                'quantity' => 1,
                'shipping_method' => 'Kurir',
                'purchase_location' => 'Jepang',
                'delivery_location' => 'Jadetabek',
                'price' => 100000,
                'user_id' => 1,
                'traveller_id' => 2,
                'status_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Teh Green tea Jointripnuri',
                'description' => 'Teh Green Titip 14 orihro night diet tea Tolong cek expired terjauh ya. Prefer tiba seblm mid jan 2018 Utk mba Nuri',
                'quantity' => 1,
                'shipping_method' => 'Kurir',
                'purchase_location' => 'Jepang',
                'delivery_location' => 'Bandung',
                'price' => 90000,
                'user_id' => 1,
                'traveller_id' => 3,
                'status_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('requests')->insert($reviews);
    }
}