<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'name' => 'Open',
                'description' => 'Request tersedia bagi traveller untuk memberikan sebuah penawaran.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Offered',
                'description' => 'Seorang traveller memberi tawaran, dan sedang menunggu tawaran diterima atau ditolak.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Pending Purchase',
                'description' => 'Requester telah membayar deposit dan barang belum dibeli oleh traveller.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Pending Delivery',
                'description' => 'Barang telah dibeli traveller, dan sedang menunggu proses pengiriman.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Pending Acknowledgement',
                'description' => 'Barang kamu dalam proses pengiriman.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Completed',
                'description' => 'Barang telah diterima oleh requester, dan traveller telah menerima pembayaran.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        DB::table('status')->insert($status);
    }
}